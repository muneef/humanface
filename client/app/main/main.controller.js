'use strict';

angular.module('idea1App')
  .controller('MainCtrl', function($scope, $modal, $http, socket) {
    $scope.awesomeThings = [];

    // $http.get('/api/things').then(function(response) {
    //   $scope.awesomeThings = response.data;
    //   socket.syncUpdates('thing', $scope.awesomeThings);
    // });


    $scope.search = 'garden';

    $scope.open = function (size) {

      
    }
    
    // $scope.change = function(){
      if($scope.search == ''){
        $scope.results = []
      }else{

        $http.get('/assets/sample.json').then(function(response) {
          console.log(response.data);
          $scope.results = response.data;
          $scope.results.reviews_total = response.data[0];

          _.each($scope.results, function(data, key){
            // var totalReviews = (data.reviews.length) - 1;
            $scope.results[key].reviews_total = (data.reviews.length) - 1;
          })
          
          // console.log($scope.results[0].reviews[0].review);
          // trying
          socket.syncUpdates('thing', $scope.results);
        });
      }
    // }


      //
    $scope.openThis = function(selected){
      var modalInstance = $modal.open({
        // animation: $scope.animationsEnabled,
        templateUrl: 'components/content/modal.html',
        controller: 'ModalInstanceCtrl',
        size: 'l',
        resolve: {
          results: function () {
            return selected;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        console.log(selectedItem);
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }
  });



angular.module('idea1App').controller('ModalInstanceCtrl', function ($scope, $modalInstance, results) {

  // console.log(results);
  
  $scope.results = results;

  console.log(results);

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});